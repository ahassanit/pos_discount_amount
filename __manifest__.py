# -*- encoding: utf-8 -*-

{
    "name": "POS Discount Amount",
    "version": "1.0",
    "author": "Hossam Hassan",
    "license": "AGPL-3",
    "category": "Point Of Sale",
    "depends": [
        'base',
        'point_of_sale',
    ],
    "data": [
        'views/templates.xml'

    ],
    'qweb': ['static/src/xml/orderline_inherit.xml'],
    "active": False,
    "installable": True
}
