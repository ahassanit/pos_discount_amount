odoo.define('pos_discount_amount', function(require){
"use strict";
    var utils = require('web.utils');
    var round_pr = utils.round_precision;
    var models = require('point_of_sale.models');
    var screens = require('point_of_sale.screens');

    var OrderlineSuper = models.Orderline
    models.Orderline = models.Orderline.extend({
    get_all_prices:function()
    {
    var res = OrderlineSuper.prototype.get_all_prices.apply(this, arguments);

        var price_unit = this.get_unit_price() - this.get_discount();
        var taxtotal = 0;

        var product =  this.get_product();
        var taxes_ids = product.taxes_id;
        var taxes =  this.pos.taxes;
        var taxdetail = {};
        var product_taxes = [];

        _(taxes_ids).each(function(el){
            product_taxes.push(_.detect(taxes, function(t){
                return t.id === el;
            }));
        });
        product_taxes = _.map(product_taxes, this._map_tax_fiscal_position.bind(this));

        var all_taxes = this.compute_all(product_taxes, price_unit, this.get_quantity(), this.pos.currency.rounding);
        _(all_taxes.taxes).each(function(tax) {
            taxtotal += tax.amount;
            taxdetail[tax.id] = tax.amount;
        });
        return {
            "priceWithTax": all_taxes.total_included,
            "priceWithoutTax": all_taxes.total_excluded,
            "tax": taxtotal,
            "taxDetails": taxdetail,
        };
    },

    get_base_price: function(){
    var res = OrderlineSuper.prototype.get_base_price.apply(this, arguments);
    var rounding = this.pos.currency.rounding;
    return round_pr((this.get_unit_price() - this.get_discount()) * this.get_quantity(), rounding);
    },

    set_discount: function(discount){
        var disc = Math.max(parseFloat(discount) || 0, 0);
        this.discount = disc;
        this.discountStr = '' + disc;
        this.trigger('change',this);
    },

    })

    var OrderSuper = models.Order

    models.Order = models.Order.extend({

    get_total_discount: function() {
    var res = OrderSuper.prototype.get_total_discount.apply(this, arguments);
        return round_pr(this.orderlines.reduce((function(sum, orderLine) {
            return sum + (orderLine.get_discount() * orderLine.get_quantity());
        }), 0), this.pos.currency.rounding);
    },
    })


    screens.NumpadWidget.include({
        clickAppendNewChar: function(event) {
        var newChar;
        newChar = event.currentTarget.innerText || event.currentTarget.textContent;
        console.log("newChar12", newChar)
        if(newChar == '.' && this.state.get('mode') == 'quantity'){
            return false
        }
        return this.state.appendNewChar(newChar);
    },
    })

});